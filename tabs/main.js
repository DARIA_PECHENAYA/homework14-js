Array.from(document.getElementsByClassName('tabs-item-label')).forEach(tab => {
  tab.onclick = (event) => {
        // const tabBlockId = event.target.getAttribute('data-item-id');
    const tabBlockId = event.target.dataset['itemId'];
    const tabBlock = document.getElementById(tabBlockId);

    Array.from(document.getElementsByClassName('tabs-block')).forEach(block => {
      if(block.classList.contains('active')){
        block.classList.remove('active');
      }
    });

    tabBlock.classList.add('active');
  };
});
const active = 'active';

Array.from(document.getElementsByClassName('tabs-item-label')).forEach(tab => {
  tab.onclick = (event) => {
    const tabBlockId = event.target.dataset['itemId'];
    const tabBlock = document.getElementById(tabBlockId);

    deActivateTabs('tabs-block');
    tabBlock.classList.add(active);

    deActivateTabs('tabs-item-label');
    event.target.classList.add(active);
  };
});

function deActivateTabs(className) {
  Array.from(document.getElementsByClassName(className)).forEach(block => {
    if(block.classList.contains(active)){
      block.classList.remove(active);
    }
  });
}
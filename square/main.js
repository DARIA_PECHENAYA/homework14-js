const w = 500;
const h = 500;
const sW = 50;
const sH = 50;
const stepW = 50;
const stepH = 50;
let direction = 0;

const s = document.getElementById('square');

s.onmouseover = (event) => {
  const oldTop = parseInt(event.target.style.top.replace('px', '')) || 0;
  const oldLeft = parseInt(event.target.style.left.replace('px', '')) || 0;
  let newTop = oldTop;
  let newLeft = oldLeft;
  
  switch(direction) {
  	case 0: 
     if(oldLeft + stepW <= w - sW) {
        newLeft += stepW;
      } 
     if( newLeft >= w - sW) {
				direction = 1;
      }
      break;
    case 1:
      if (oldTop + stepH <= h -sH){
	  			newTop += stepH;
  		} 
      if( newTop >= h - sH){
  			direction = 2;
  		}
    	break;
    case 2:
     if (oldLeft + stepW >= 0) {
  			newLeft -= stepW;
      } 
     if( newLeft <= 0) {
				direction = 3;
      } 
    	break;
    case 3:
     if (oldTop + stepH >= 0) {
				newTop -= stepH;  
      }
      if( newTop <= 0){
				direction = 0;
      }
    	break;
  } 
 
  event.target.style.top = newTop + 'px';
  event.target.style.left = newLeft + 'px';
  
}